package com.utilities.stevenmugo.utilities.fragmentss.utilities.network_requests;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by stevenmugo on 10/26/16.
 */

public class ServerRequest  {

    private static final String APP_ID = "149c99fb003a027db2dc77d1f960e994";
    private String url;
    private String units;
    double lat = 40.712774, lon = -74.006091;

    public ServerRequest(){
        units = "imperial";
        /*url = String.format("http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=%s&appid=%s",
                lat, lon, units, APP_ID);*/
        url = String.format("http://api.openweathermap.org/data/2.5/forecast/city?id=524901&APPID=",APP_ID);

        new GetWeatherTask("Hello").execute(url);
    }

    private class GetWeatherTask extends AsyncTask<String, Void, String> {
        private String textView;

        public GetWeatherTask(String textView) {
            this.textView = textView;
        }

        @Override
        protected String doInBackground(String... strings) {
            String weather = "UNDEFINED";
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder builder = new StringBuilder();

                String inputString;
                while ((inputString = bufferedReader.readLine()) != null) {
                    builder.append(inputString);
                }

                JSONObject topLevel = new JSONObject(builder.toString());
                JSONObject main = topLevel.getJSONObject("main");
                weather = String.valueOf(main.getDouble("temp"));

                urlConnection.disconnect();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return weather;
        }

        @Override
        protected void onPostExecute(String temp) {
            //textView.setText("Current Weather: " + temp);
            textView = "Current Weather: " + temp;
        }
    }

}
