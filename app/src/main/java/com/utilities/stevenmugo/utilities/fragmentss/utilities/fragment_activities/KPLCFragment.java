package com.utilities.stevenmugo.utilities.fragmentss.utilities.fragment_activities;
/**
 * Created by stevenmugo on 10/25/16.
 */

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;

import java.util.ArrayList;
import java.util.Arrays;


public class KPLCFragment extends Fragment {
    //Variables
    View view;
    private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;

    public KPLCFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_kplc_main, container, false);
        mainListView = (ListView) view.findViewById( R.id.mainListView );

        String[] planets = new String[] { "Post Pay", "Pre-Pay", "Check Balance", "Okoa Stima"};
        ArrayList<String> planetList = new ArrayList<String>();
        planetList.addAll( Arrays.asList(planets) );
        listAdapter = new ArrayAdapter<String>(getContext(), R.layout.simplerow, planetList);
        mainListView.setAdapter(listAdapter);


        return view;
    }
}

