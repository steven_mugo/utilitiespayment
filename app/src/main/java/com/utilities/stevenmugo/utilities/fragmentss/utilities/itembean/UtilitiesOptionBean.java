package com.utilities.stevenmugo.utilities.fragment.utilities.itembean;

/**
 * Created by stevenmugo on 10/19/16.
 */

public class UtilitiesOptionBean {
    String option_title;
    String option_description;
    int option_icon_image;

    //If Dynamic Use the below Variables
    //String icon_image_url;
    //Bitmap image_bitmap;

    public String getTitle() {
        return option_title;
    }

    public void setTitle(String title) {
        this.option_title = title;
    }

    public String getDescription(){
        return option_description;
    }

    public void setDescription(String description){
        this.option_description = description;
    }

    public int getIconImage(){
        return option_icon_image;
    }

    public void setIconImage(int icon_image){
        this.option_icon_image = icon_image;
    }
}
