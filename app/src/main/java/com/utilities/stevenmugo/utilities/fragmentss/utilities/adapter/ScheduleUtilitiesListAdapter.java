package com.utilities.stevenmugo.utilities.fragmentss.utilities.adapter;

/**
 * Created by stevenmugo on 1/12/17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.utils.CalendarUtils;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean.ScheduleUtilitiesBean;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.nwscoption.NWSCHomeOptions;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.schedule_action.SetPaymentScheduleDate;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.zukuoption.ZUKUHomeOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ScheduleUtilitiesListAdapter extends BaseAdapter {
    private String TAG = "ScheduleUtilities/adapter";
    ArrayList<Object> utilities_list;
    private ViewHolder holder;

    private Activity context;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;

    CustomCalendarView calendarView;
    private TextView selectedDateTv;
    Date date = new Date();

    String selected_date = "";

    //Default Constructor
    public ScheduleUtilitiesListAdapter(){

    }

    public ScheduleUtilitiesListAdapter(Activity context, ArrayList<Object> utilitiesList) {
        super();
        this.context = context;
        this.utilities_list = utilitiesList;

        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return utilities_list.size();
    }

    @Override
    public Object getItem(int position) {
        return utilities_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        if (convertView == null) {
            final String[] selected_utility = {"","","",""};
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.schedule_utilities_list_item_row, null);

            holder.thumb_image = (ImageView) convertView
                    .findViewById(R.id.list_image);

            holder.title = (TextView) convertView.findViewById(R.id.title);

            holder.description = (TextView) convertView
                    .findViewById(R.id.title_description);

            holder.edit_schedule = (ImageView) convertView.findViewById(R.id.edit_schedule);
            final View finalConvertView = convertView;
            holder.edit_schedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context,"Edit Schedule",Toast.LENGTH_LONG).show();
                    switch (position){
                    case 0://KLPC menu option Items
                        selected_utility[0] = "KPLC";
                        break;
                    case 1://NWSC menu option Items
                        selected_utility[1] = "NWSC";
                        break;
                    case 2://ZUKU menu option Items
                        selected_utility[2] = "ZUKU";
                        break;
                    case 3://DSTV menu option Items
                        selected_utility[3] = "DSTV";
                        break;
                    }
                    displayCalendor(selected_utility[position], position, finalConvertView);
                    //Intent intent = new Intent(context, SetPaymentScheduleDate.class);
                    //context.startActivity(intent);
                }
            });
            holder.remove_schedule = (ImageView) convertView.findViewById(R.id.remove_schedule);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ScheduleUtilitiesBean bean = (ScheduleUtilitiesBean) utilities_list.get(position);

        holder.thumb_image.setImageResource(bean.getIconImage());
        holder.title.setText(bean.getTitle());
        holder.description.setText(bean.getDescription());

        itemListListener(convertView,position);
        return convertView;
    }

    public static class ViewHolder {
        ImageView thumb_image;
        TextView title;
        TextView description;

        ImageView edit_schedule;
        ImageView remove_schedule;
    }

    private void itemListListener(View current_view, final int position){
        current_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w(TAG,"Position is: "+position);
                switch (position){
                    case 0://KLPC menu option Items
                        //context.startActivity(new Intent(context, KPLCHomeOptions.class));
                        Log.e("Clicked View"," KPLC View");

                        //new ServerRequest();
                        break;
                    /*case 1://NWSC menu option Items
                        context.startActivity(new Intent(context, NWSCHomeOptions.class));
                        break;
                    case 2://ZUKU menu option Items
                        context.startActivity(new Intent(context, ZUKUHomeOptions.class));
                        break;
                    case 3://DSTV menu option Items
                        context.startActivity(new Intent(context, DSTVHomeOptions.class));
                        break;*/
                    default:
                        break;
                }
            }
        });
    }

    //Custom Calendar Alert Dialog
    private void displayCalendor(String utility_title, final int position, View convertView){
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = context.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.set_payment_dialog_layout, null);
        dialogBuilder.setView(dialogView);

        //final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        /*dialogBuilder.setTitle("Set Utility Payment Schedule Date?");
        dialogBuilder.setMessage("Set "+utility_title+" payment schedule?");
        dialogBuilder.setIcon(R.drawable.ic_schedule);
        dialogBuilder.setView(R.layout.set_payment_dialog_layout);*/


        //final View dialog_View = inflater.inflate(R.layout.set_payment_dialog_layout, null);
        selectedDateTv = (TextView) dialogView.findViewById(R.id.selected_date);
        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) dialogView.findViewById(R.id.calendar_view);

        //Initialize calendar with date
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                //ScheduleUtilitiesListAdapter.this.date = date;
                if (!CalendarUtils.isPastDay(date)) {
                    SimpleDateFormat df = new SimpleDateFormat("EEE, MMM d, yyyy");
                    selectedDateTv.setText("Scheduled date is " + df.format(date));
                    selected_date = ""+df.format(date);
                    Log.e("Date",""+selected_date);
                } else {
                    selectedDateTv.setText("Selected date is disabled!");
                }
            }

            @Override
            public void onMonthChanged(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(context, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });



        dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();

                switch (position){
                    case 0:
                        SimpleDateFormat df = new SimpleDateFormat("EEE, MMM d, yyyy");
                        Log.e("Clicked View"," KPLC View Position "+selected_date);
                        //holder.
                        //itemListListener(view,position);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
                /*if (saving_category == 0) {
                    saving_id = "0";
                    saving_week = "1";
                    saving_amount = "10";
                    saving_point = "10";
                    saving_addition_status[0] = "Ksh. 10, has been saved successfully.";
                    dialogBuilder.setMessage("Ksh. 10, has been saved successfully.");
                } else if (saving_category == 1) {
                    saving_id = "1";
                    saving_week = "1";
                    saving_amount = "200";
                    saving_point = "6";
                    saving_addition_status[0] = "Ksh. 200, has been saved successfully.";
                    dialogBuilder.setMessage("Ksh. 200, has been saved successfully");
                } else if (saving_category == 2) {
                    saving_id = "2";
                    saving_week = "1";
                    saving_amount = "100";
                    saving_point = "3";
                    saving_addition_status[0] = "Ksh. 100, has been saved successfully.";
                    dialogBuilder.setMessage("Ksh. 100, has been saved successfully");
                }


                savingsdbHelper = new SavingsDatabaseHelper(mContext);
                savingsdbHelper.open();
                add_savings_response[0] = savingsdbHelper.insertSavings(saving_id, saving_week, saving_amount, saving_point, created_on);
                if (add_savings_response[0] == -1) {

                    Toast.makeText(mContext, saving_addition_status[0]+": "+saving_category, Toast.LENGTH_SHORT).show();
                } else {

                    Toast.makeText(mContext, saving_addition_status[0], Toast.LENGTH_SHORT).show();
                }

                */
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                //add_savings_response[0] = -2;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
