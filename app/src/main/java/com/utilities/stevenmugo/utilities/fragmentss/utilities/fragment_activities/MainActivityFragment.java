package com.utilities.stevenmugo.utilities.fragmentss.utilities.fragment_activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.adapter.UtilitiesListAdapter;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean.UtilitiesBean;

import java.util.ArrayList;

/**
 * Created by stevenmugo on 10/19/16.
 */
public class MainActivityFragment extends Fragment {
    //Variables
    ListView utilities_list;
    UtilitiesListAdapter adapter;
    private ArrayList<Object> utility_item_list = null;
    private UtilitiesBean bean = null;

    View view;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);

        prepareUtilitiesList();

        utilities_list = (ListView) view.findViewById(R.id.utilities_list);
        adapter = new UtilitiesListAdapter(this.getActivity(), utility_item_list);
        utilities_list.setAdapter(adapter);

        return view;
    }

    public void prepareUtilitiesList() {
        utility_item_list = new ArrayList<Object>();

        // Add items to list
        addObjectToList(R.drawable.kplc, "KPLC", "The Kenya Power & Lightning Co. Ltd.");
        addObjectToList(R.drawable.nwsc, "NWSC", "Improving Reliability");
        addObjectToList(R.drawable.zuku, "ZUKU", "Amazing!");
        addObjectToList(R.drawable.dstv, "DSTV", "So much more");
    }

    // Add one item into the Array List
    public void addObjectToList(int icon_image, String title, String description) {
        bean = new UtilitiesBean();
        bean.setIconImage(icon_image);
        bean.setTitle(title);
        bean.setDescription(description);
        utility_item_list.add(bean);
    }
}
