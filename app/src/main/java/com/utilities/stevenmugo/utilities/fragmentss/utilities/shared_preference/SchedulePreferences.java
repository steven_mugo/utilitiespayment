package com.utilities.stevenmugo.utilities.fragmentss.utilities.shared_preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by stevenmugo on 1/12/17.
 */

public class SchedulePreferences {
    public static final String SP_NAME = "UserDetails";
    private static final String TAG = "Shared Preferences";
    static SharedPreferences localDatabase;

    public SchedulePreferences(Context context)
    {
        localDatabase = context.getSharedPreferences(SP_NAME , Context.MODE_PRIVATE);
    }

    public void storeData(PaymentSchedule payment_schedule)
    {
        SharedPreferences.Editor spEditor = localDatabase.edit();
        spEditor.putString("mobile_number" , payment_schedule.mobile_number);
        spEditor.putString("pass_code" , payment_schedule.pass_code);

        //spEditor.putString("locationKey","Budalangi");
        spEditor.commit();
    }

    public static PaymentSchedule getLoggedInUser()
    {
        //Sometimes its in-accessible WHY?????
        String mobile_number = localDatabase.getString("mobile_number" , "");
        String pass_code = localDatabase.getString("pass_code" , "");


        Log.e(TAG, "Preferences: " + mobile_number + " " + pass_code);

        PaymentSchedule stored_payment_schedule = new PaymentSchedule(mobile_number , pass_code);
        return stored_payment_schedule;
    }

    public void setUserLoggedIn(boolean loggedIn)
    {
        SharedPreferences.Editor spEditor = localDatabase.edit();
        spEditor.putBoolean("loggedIn", loggedIn);
        spEditor.commit();

    }

    public boolean getUserLoggedIn()
    {
        if(localDatabase.getBoolean("loggedIn" , false))
            return true;
        else
            return false;
    }

    public void clearData()
    {
        SharedPreferences.Editor spEditor = localDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }

}
