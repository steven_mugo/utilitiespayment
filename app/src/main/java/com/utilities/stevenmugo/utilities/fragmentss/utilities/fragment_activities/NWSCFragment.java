package com.utilities.stevenmugo.utilities.fragmentss.utilities.fragment_activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;


public class NWSCFragment extends Fragment {
    //Variables
    View view;

    public NWSCFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_nwsc_main, container, false);

        return view;
    }
}

