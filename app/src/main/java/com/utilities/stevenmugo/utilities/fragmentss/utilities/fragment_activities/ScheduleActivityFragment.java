package com.utilities.stevenmugo.utilities.fragmentss.utilities.fragment_activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.adapter.ScheduleUtilitiesListAdapter;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.adapter.UtilitiesListAdapter;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean.ScheduleUtilitiesBean;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean.UtilitiesBean;

import java.util.ArrayList;

/**
 * Created by stevenmugo on 1/12/17.
 */

public class ScheduleActivityFragment extends Fragment {
    //Variables
    ListView schedule_utilities_list;
    ScheduleUtilitiesListAdapter adapter;
    private ArrayList<Object> utility_schedule_item_list = null;
    private ScheduleUtilitiesBean bean = null;

    View view;

    public ScheduleActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_schedule_action, container, false);

        prepareUtilitiesList();

        schedule_utilities_list = (ListView) view.findViewById(R.id.schedule_utilities_list);
        adapter = new ScheduleUtilitiesListAdapter(this.getActivity(), utility_schedule_item_list);
        schedule_utilities_list.setAdapter(adapter);

        return view;
    }

    public void prepareUtilitiesList() {
        utility_schedule_item_list = new ArrayList<Object>();

        // Add items to list
        addObjectToList(R.drawable.kplc, "KPLC", "30th January 2017");
        addObjectToList(R.drawable.nwsc, "NWSC", "11th February 2017");
        addObjectToList(R.drawable.zuku, "ZUKU", "26th January 2017");
        addObjectToList(R.drawable.dstv, "DSTV", "not set");
    }

    // Add one item into the Array List
    public void addObjectToList(int icon_image, String title, String description) {
        bean = new ScheduleUtilitiesBean();
        bean.setIconImage(icon_image);
        bean.setTitle(title);
        bean.setDescription(description);
        utility_schedule_item_list.add(bean);
    }
}
