package com.utilities.stevenmugo.utilities.fragmentss.utilities.fragment_activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;

public class ZUKUFragment extends Fragment {
    //Variables
    View view;

    public ZUKUFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_zuku_main, container, false);

        return view;
    }
}


