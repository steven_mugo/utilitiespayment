package com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean;

/**
 * Created by stevenmugo on 10/19/16.
 */

public class UtilitiesBean {
    String utilities_title;
    String utilities_description;
    int utilities_icon_image;

    //If Dynamic Use the below Variables
    //String icon_image_url;
    //Bitmap image_bitmap;

    public String getTitle() {
        return utilities_title;
    }

    public void setTitle(String title) {
        this.utilities_title = title;
    }

    public String getDescription(){
        return utilities_description;
    }

    public void setDescription(String description){
        this.utilities_description = description;
    }

    public int getIconImage(){
        return utilities_icon_image;
    }

    public void setIconImage(int icon_image){
        this.utilities_icon_image = icon_image;
    }
}
