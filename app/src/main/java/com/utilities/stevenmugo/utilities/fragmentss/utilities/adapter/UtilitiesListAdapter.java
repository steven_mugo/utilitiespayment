package com.utilities.stevenmugo.utilities.fragmentss.utilities.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.utilities.stevenmugo.utilities.fragmentss.utilities.R;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.dstvoption.DSTVHomeOptions;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.itembean.UtilitiesBean;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.kplcoption.KPLCHomeOptions;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.network_requests.ServerRequest;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.nwscoption.NWSCHomeOptions;
import com.utilities.stevenmugo.utilities.fragmentss.utilities.zukuoption.ZUKUHomeOptions;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by stevenmugo on 10/19/16.
 */

public class UtilitiesListAdapter extends BaseAdapter {
    private String TAG = "Utilities/adapter";
    ArrayList<Object> utilities_list;
    private ViewHolder holder;

    private Activity context;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater = null;

    //Default Constructor
    public UtilitiesListAdapter(){

    }

    public UtilitiesListAdapter(Activity context, ArrayList<Object> utilitiesList) {
        super();
        this.context = context;
        this.utilities_list = utilitiesList;

        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return utilities_list.size();
    }

    @Override
    public Object getItem(int position) {
        return utilities_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.utilities_list_item_row, null);

            holder.thumb_image = (ImageView) convertView
                    .findViewById(R.id.list_image);

            holder.title = (TextView) convertView.findViewById(R.id.title);

            holder.description = (TextView) convertView
                    .findViewById(R.id.title_description);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        UtilitiesBean bean = (UtilitiesBean) utilities_list.get(position);

        holder.thumb_image.setImageResource(bean.getIconImage());
        holder.title.setText(bean.getTitle());
        holder.description.setText(bean.getDescription());

        itemListListener(convertView,position);
        return convertView;
    }

    public static class ViewHolder {
        ImageView thumb_image;
        TextView title;
        TextView description;
    }

    private void itemListListener(View current_view, final int position){
        current_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w(TAG,"Position is: "+position);
                switch (position){
                    case 0://KLPC menu option Items
                        context.startActivity(new Intent(context, KPLCHomeOptions.class));
                        new ServerRequest();
                        break;
                    case 1://NWSC menu option Items
                        context.startActivity(new Intent(context, NWSCHomeOptions.class));
                        break;
                    case 2://ZUKU menu option Items
                        context.startActivity(new Intent(context, ZUKUHomeOptions.class));
                        break;
                    case 3://DSTV menu option Items
                        context.startActivity(new Intent(context, DSTVHomeOptions.class));
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
