package com.utilities.stevenmugo.utilities.fragmentss.utilities.shared_preference;

import android.util.Log;

/**
 * Created by stevenmugo on 1/12/17.
 */

public class PaymentSchedule {

    private static final String TAG = "Schedule Instance";
    public String mobile_number , pass_code;

    public PaymentSchedule(String mobile_number , String pass_code , String username , String password, String collector_id, String county_id, String subcounty_id)
    {
        this.mobile_number = mobile_number;
        this.pass_code = pass_code ;

        Log.e(TAG, "Saving User Data: " + mobile_number + " " + pass_code);
    }

    public PaymentSchedule(String mobile_number , String pass_code)
    {
        this.mobile_number = mobile_number;
        this.pass_code = pass_code;
    }

}
